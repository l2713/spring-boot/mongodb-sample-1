package com.lab.spring.boot.mongdb;

import com.lab.spring.boot.mongdb.repository.UserRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication
@EnableMongoRepositories(basePackageClasses = UserRepository.class)
public class MongoDbSample1Application {

	public static void main(String[] args) {
		SpringApplication.run(MongoDbSample1Application.class, args);
	}

}

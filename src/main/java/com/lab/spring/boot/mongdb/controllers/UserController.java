package com.lab.spring.boot.mongdb.controllers;

import com.lab.spring.boot.mongdb.model.User;
import com.lab.spring.boot.mongdb.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springdoc.api.annotations.ParameterObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
public class UserController {
    private final UserRepository userRepository;

    @GetMapping
    public Page<User> findAll(@PageableDefault(size = 20) @ParameterObject Pageable pageRequest) {
        return userRepository.findAll(pageRequest);
    }

    @PostMapping
    public User createNew(@RequestBody User user) {
        return userRepository.save(user);
    }
}

package com.lab.spring.boot.mongdb.repository;

import com.lab.spring.boot.mongdb.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserRepository extends MongoRepository<User, String> {
}
